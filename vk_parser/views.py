#coding=utf-8

from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import MainForm
from .tasks import do_big_deal
from django.conf import settings
import os
from django.http import Http404
import datetime

def send_messages(request):
    if request.method == 'POST':
        form = MainForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            name = form.cleaned_data['name']
            ira_data = form.cleaned_data['ira_data']

            do_big_deal.delay(email, name, ira_data)

            return HttpResponseRedirect('/thanks/')
    else:
        form = MainForm()

    return render(request, 'vk_parser/index.html', {'form': form})

def get_messages(request, hash):
    if "{0}.json".format(hash) in os.listdir('{0}/static'.format(settings.BASE_DIR)):
        return render(request, 'vk_parser/messages.html', {'json': hash+'.json'})
    else:
        raise Http404('Страница не существует')

def delete_json(hash):
    os.remove('{0}/static/{1}.json'.format(settings.BASE_DIR, hash))

def delete_messages(request, hash):
    delete_json(hash)
    return HttpResponseRedirect('/')