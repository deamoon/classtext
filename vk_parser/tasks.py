#coding=utf-8

import json
import string
import random
from celery import task
from django.conf import settings
from django.core.mail import send_mail
from .parser import get_vk_messages
from celery.utils.log import get_task_logger
from collections import Counter

logger = get_task_logger('vk_parser')

def super_ML_algo(ira_data):
    """Мы нашил 5 самых популярных букв"""
    return str(Counter(ira_data).most_common(5))

@task
def do_big_deal(email, name, ira_data):
    recipients = [email]    
    subject = 'ClassText'
    html_message = """<b>Спасибо</b>, {0}, Ваши данные проанализированы. {1}""".format(name, super_ML_algo(ira_data))
    message = """Спасибо, {0}, Ваши данные проанализированы. {1}""".format(name, super_ML_algo(ira_data))

    send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, recipients, html_message=html_message)