from django import forms

class MainForm(forms.Form):
    email = forms.EmailField()
    name = forms.CharField(max_length=100)
    ira_data = forms.CharField(required=False)