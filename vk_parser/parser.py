#!/usr/bin/python
#coding=utf-8

import vk
import math
import time
import datetime
import requests
from django.conf import settings

class VkParser:
    """ Download all user's messages"""

    def __init__(self, access_token, end_time):
        self.end_time = end_time
        self.app_id = settings.VK_APP_ID
        self.access_token = access_token
        self.vk_api = vk.API(self.app_id, access_token=self.access_token)

        for _ in range(5):
            try:
                time.sleep(1)
                self.count_not_out_messages = int(self.vk_api.messages.get(out=0)['count'])
                print "[count_not_out_messages]"
                break
            except Exception as e:
                print "[count_not_out_messages]", e

        for _ in range(5):
            try:
                time.sleep(1)
                self.count_out_messages = int(self.vk_api.messages.get(out=1)['count'])
                print "[count_out_messages]"
                break
            except Exception as e:
                print "[count_out_messages]", e

        self.messages = []
        self.__get_friends()

    @staticmethod
    def convert_friend(friend):
        name = (friend['first_name'] + ' ' + friend['last_name']).strip()
        if not name:
            name = friend['nickname'].strip()
        result = {
            'name': name,
            'domain': friend['domain'].strip(),
            'id': friend['id'],
        }
        return result

    def __get_friends(self):
        self.friends = {}

        for _ in range(5):
            try:
                time.sleep(1)
                vk_friends = self.vk_api.friends.get(fields='nickname,domain')
                print "[FRIENDS]"
                break
            except Exception as e:
                print "[FRIENDS]", e

        for friend in vk_friends['items']:
            self.friends[friend['id']] = self.convert_friend(friend)

    def __parse_messages(self, out=0):
        count = 200
        test_count = 999999
        if out:
            count_messages = self.count_out_messages
        else:
            count_messages = self.count_not_out_messages

        for i in range(0, min(int(math.ceil(count_messages / float(count))), test_count)):
            for _ in range(5):
                try:
                    time.sleep(0.3)
                    cur_messages = self.vk_api.messages.get(count=count, offset=i*count, out=out)[u'items']
                    print "[MESSAGES]", count, i*count, out

                    if cur_messages and self.end_time and \
                      datetime.datetime.fromtimestamp(cur_messages[0]['date']) < \
                      datetime.datetime.now() - self.end_time:
                        return

                    self.messages += cur_messages
                    break
                except Exception as e:
                    print "[MESSAGES]", e


    def start_parsing(self):
        self.__parse_messages()
        self.__parse_messages(out=1)
        self.messages.sort(key=lambda x: -x['date'])

def get_access_token(code):
    url = "https://oauth.vk.com/access_token?client_id={0}&client_secret={1}&redirect_uri=https://oauth.vk.com/blank.html&code={2}".format(settings.VK_APP_ID, settings.VK_APP_SECRET, code)

    r = requests.get(url)
    access_token = r.json()['access_token']
    print "[access_token]", access_token

    return access_token

def get_vk_messages(code, end_time=None):
    access_token = get_access_token(code)
    parser = VkParser(access_token, end_time)
    parser.start_parsing()

    unknown_users = []
    for message in parser.messages:
        if message['user_id'] in parser.friends:
            user = parser.friends[message['user_id']]
            message['user_id'] = u"<a href=\"http://vk.com/{0}\">{1}</a>".format(user['domain'], user['name'])
        else:
            unknown_users.append(message['user_id'])
        message['date'] = datetime.datetime.fromtimestamp(message['date']).strftime('%Y-%m-%d %H:%M:%S')
    unknown_users = list(set(unknown_users))

    step = 500
    known_friends = {}
    print "[unknown_users]", len(unknown_users)

    for i in range(0, int(math.ceil(len(unknown_users) / float(step)))):
        new_users = ','.join(map(str, unknown_users[i*step:(i+1)*step]))

        for _ in range(5):
            try:
                time.sleep(1)
                users = parser.vk_api.users.get(fields='nickname,domain', user_ids=new_users)
                print "USERS"

                for user in map(parser.convert_friend, users):
                    known_friends[user['id']] = u"<a href=\"http://vk.com/{0}\">{1}</a>".format(user['domain'], user['name'])
                break
            except Exception as e:
                print "[USERS]", e
                pass

    for message in parser.messages:
        if message['user_id'] in known_friends:
            message['user_id'] = known_friends[message['user_id']]

    messages = []
    for message in parser.messages:
        messages.append([message.get(k, None) for k in ('body', 'date', 'user_id', 'out', 'title')])

    dyno_res = {}
    dyno_res['data'] = messages

    return dyno_res

if __name__ == '__main__':
    get_vk_messages('fa8218122ca3e7938f')
    # from vk_parser.parser import get_vk_messages
    # a = get_vk_messages('48956e42c56a693314')