"""vk_manga URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""

from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import TemplateView

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^thanks/$', TemplateView.as_view(template_name='vk_parser/thanks.html'), name='thanks'),
    url(r'^$', 'vk_parser.views.send_messages', name='index'),
    url(r'^example/$', TemplateView.as_view(template_name='vk_parser/example.html'), name='example'),
    url(r'^messages/(?P<hash>\w+)/$', 'vk_parser.views.get_messages', name='get_messages'),
    url(r'^messages/(?P<hash>\w+)/delete$', 'vk_parser.views.delete_messages', name='delete_messages'),
]
